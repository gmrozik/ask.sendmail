# -*- coding: utf-8 -*-

from email import encoders
from email.mime.application import MIMEApplication
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import email.utils
import mimetypes
import os
import smtplib
import sys

sender = 'author@example.com'
recipient = sys.argv[1]
attachments = sys.argv[2:]

# Create simple text message
if not attachments:
    msg = MIMEText('This is the body of simple text message.')
else:
    msg = MIMEMultipart()
    msg.attach(MIMEText('This is the body of the multipart message.'))

msg['To'] = email.utils.formataddr(('Recipient', recipient))
msg['From'] = email.utils.formataddr(('Author', sender))
msg['Subject'] = 'Simple test message'

for attachment in attachments:
    filename = os.path.basename(attachment)
    ctype, encoding = mimetypes.guess_type(attachment)
    if ctype is None or encoding is not None:
        ctype = 'application/octet-stream'
    maintype, subtype = ctype.split('/', 1)
    if maintype == 'text':
        fp = open(attachment)
        # Note: we should handle calculating the charset
        part = MIMEText(fp.read(), _subtype=subtype)
        fp.close()
    elif maintype == 'image':
        fp = open(attachment, 'rb')
        part = MIMEImage(fp.read(), _subtype=subtype)
        fp.close()
    elif maintype == 'audio':
        fp = open(attachment, 'rb')
        part = MIMEAudio(fp.read(), _subtype=subtype)
        fp.close()
    elif maintype == 'application':
        fp = open(attachment, 'rb')
        part = MIMEApplication(fp.read(), _subtype=subtype)
        fp.close()
    else:
        fp = open(attachment, 'rb')
        part = MIMEBase(maintype, subtype)
        part.set_payload(fp.read())
        fp.close()
        encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment', filename=filename)
    msg.attach(part)

msg.attach(MIMEText('This is the second body of the multipart message.'))

local = smtplib.SMTP('127.0.0.1', 1025)
gmail = smtplib.SMTP('smtp.gmail.com', 587)

server = local

server.set_debuglevel(True) # show communication with the server

if server == gmail:
    sender = 'xyz@gmail.com'
    password = '***'
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(sender, password)

try:
    server.sendmail(sender, [recipient], msg.as_string())
finally:
    server.quit()