# -*- coding: utf-8 -*-

from email import message_from_string
from mailbox import Maildir
import asyncore
import smtpd
import os
from email.mime.text import MIMEText

class InvalidUser(Exception):
	pass


class UserDatabase(object):
	
	def __init__(self, filename):
		self.users = self._parse_file(filename)
	
	def contains(self, username):
		return username in self.users # FIXME: implement asterisk scenario
	
	def _parse_file(self, filename):
		result = []
		with open(filename) as f:
			for line in f:
				line = line.strip()
				if line:
					if self._valid_user(line):
						result.append(line)
					else:
						raise InvalidUser(line)
		return result
	
	def _valid_user(self, username):
		return True # FIXME: implement this as regex


class CustomMailbox(object):
	
	def __init__(self, userdb, dirname):
		self.userdb = userdb
		self.mailboxes = {}
		dirname = os.path.abspath(dirname)
		if not os.path.exists(dirname):
			os.mkdir(dirname)
		elif not os.path.isdir(dirname):
			raise Exception('Cannot create mailbox directory')
		self.root_dir = dirname
	
	def add(self, recipient, message):
		mailbox = self._get_mailbox(recipient)
		mailbox.add(message)
	
	def _get_mailbox(self, recipient):
		# FIXME: implement asterisk scenario
		if not recipient in self.mailboxes:
			path = self._maildir_path(recipient)
			self.mailboxes[recipient] = Maildir(path)
		return self.mailboxes[recipient]
	
	def _maildir_path(self, recipient):
		return os.path.join(self.root_dir, recipient)


class CustomSMTPChannel(smtpd.SMTPChannel):

	def smtp_RCPT(self, arg):
		if not self._SMTPChannel__mailfrom:
			self.push('503 Error: need MAIL command')
			return
		address = self._SMTPChannel__getaddr('TO:', arg) if arg else None
		if not address:
			self.push('501 Syntax: RCPT TO: <address>')
			return
		if not self._SMTPChannel__server.mailbox_available(address):
			self.push('550 Error: mailbox unavailable')
			return
		self._SMTPChannel__rcpttos.append(address)
		self.push('250 Ok')


class CustomSMTPServer(smtpd.SMTPServer):
	
	def __init__(self, localaddr, remoteaddr, userdb, mailbox, processors):
		smtpd.SMTPServer.__init__(self, localaddr, remoteaddr)
		self.userdb = userdb
		self.mailbox = mailbox
		self.processors = processors
	
	def handle_accept(self):
		pair = self.accept()
		if pair is not None:
			conn, addr = pair
			CustomSMTPChannel(self, conn, addr)
	
	def mailbox_available(self, recipient):
		return self.userdb.contains(recipient)
	
	def process_message(self, peer, mailfrom, rcpttos, data):
		message = message_from_string(data)
		for recipient in rcpttos:
			self._process_message(recipient, message)
	
	def _process_message(self, recipient, message):
		for processor in self.processors:
			message = processor.process_message(message)
		print message
		self.mailbox.add(recipient, message)


class StripExecutableProcessor(object):
	EXECUTABLE_EXTS = ['exe', 'com', 'sys']
	EXECUTABLE_SIGNATURES = [
		'4D5A'.decode('hex'), # DOS EXE
		'7F454C46'.decode('hex'), # ELF
		'E8'.decode('hex'), # ELF
		'E9'.decode('hex'), # ELF
		'EB'.decode('hex'), # ELF
		'FF'.decode('hex'), # ELF
	]
	
	def process_message(self, message):
		executables = []
		for attachment in self.attachments(message):
			if self._can_be_executable(attachment):
				executables.append(attachment)
		if executables:
			self._delete_parts(message, executables)
			stripped_files = [part.get_filename() or '(Brak nazwy)'
							  for part in executables]
			msg = self._gen_strip_message(stripped_files)
			message.attach(msg)
		return message
	
	def attachments(self, message):
		def is_declared_attachment(part):
			return ('Content-Disposition' in part and
					'attachment' in part['Content-Disposition'])
		
		def act_as_attachment(part):
			return part.get_content_maintype() != 'text'
		
		for part in message.walk():
			if part.is_multipart():
				continue
			if is_declared_attachment(part) or act_as_attachment(part):
				yield part
	
	def _can_be_executable(self, attachment):
		filename = attachment.get_filename()
		ext = os.path.splitext(filename)[1].lower()
		if ext in self.EXECUTABLE_EXTS:
			return True
		payload = attachment.get_payload(decode=True)
		for sig in self.EXECUTABLE_SIGNATURES:
			if payload.startswith(sig):
				return True
		return False
	
	def _gen_strip_message(self, stripped_files):
		text = ('Znalezlismy w Twojej wiadomosci potencjalnie '
				'niebezpieczne zalacziki: %s. Pozwolilismy sobie '
				'je usunac. Milego dnia uzytkowniku!'
				) % u', '.join(stripped_files)
		return MIMEText(text)
	
	def _delete_parts(self, message, parts):
		if message.is_multipart():
			for part in list(message._payload):
				if part in parts:
					message._payload.remove(part)
				else:
					self._delete_parts(part, parts)

if __name__=='__main__':
	# settings
	# TODO: move to separated file
	host = '127.0.0.1'
	port = 1025
	userdb_file = 'user_db.txt'
	maildir = 'maildir'
	
	userdb = UserDatabase(userdb_file)
	mailbox = CustomMailbox(userdb, maildir)
	server = CustomSMTPServer((host, port), None, userdb, mailbox, [StripExecutableProcessor()])
	print 'Server starting on %s:%d' % (host, port)
	try:
		asyncore.loop()
	except KeyboardInterrupt:
		print 'Stopping server...'